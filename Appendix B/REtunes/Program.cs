﻿using REtunes.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using REtunes.Models;

namespace REtunes
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            SelectAllCustomers(repository);
            SelectCustomerById(repository);
            SelectByName(repository);
            SelectCustomersBetweenValues(repository);
            InsertNewCustomer(repository);
            UpdateCustomer(repository);
            SelectCustomersByCountry(repository);
            SelectCustomerByTotal(repository);
            SelectCustomerFavoriteGenre(repository);
        }

        /// <summary>
        /// Gets a specific customers favorite genre from the database.
        /// </summary>
        /// <param name="repository">interface object</param>
        public static void SelectCustomerFavoriteGenre(ICustomerRepository repository)
        {
            List<CustomerGenre> numbers = repository.GetCustomersFavoriteGenre(12);
            int firstValue = numbers.First().GenreCount;
            foreach (CustomerGenre customerObj in numbers)
            {
                if (firstValue == customerObj.GenreCount)
                {
                    Console.WriteLine(customerObj.Genre + " " + customerObj.GenreCount);
                }
            }
        }

        /// <summary>
        /// Gets how many customers that comes from the same countries.
        /// </summary>
        /// <param name="repository">Interface object</param>
        public static void SelectCustomersByCountry(ICustomerRepository repository)
        {
            List<CustomerCountry> numbers = repository.GetCustomersOrderedByCountry();
            foreach (CustomerCountry customerObj in numbers)
            {
                Console.WriteLine($"{customerObj.CountryName} {customerObj.NumberOfCustomers}");
            }
        }

        /// <summary>
        /// Gets all customers from the database.
        /// </summary>
        /// <param name="repository">Interface object</param>
        public static void SelectAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        /// <summary>
        /// Gets all customers and their total spendings from the database.
        /// </summary>
        /// <param name="repository">Interface object</param>
        public static void SelectCustomerByTotal(ICustomerRepository repository)
        {
            List<CustomerSpender> numbers = repository.GetCustomersInvoiceOrderByTotal();
            foreach (CustomerSpender customerObj in numbers)
            {
                Console.WriteLine($"{customerObj.FullName} {customerObj.Total}");
            }
        }

        /// <summary>
        /// Inserts a new customer into the database, with the specific values.
        /// </summary>
        /// <param name="repository">Interface Object</param>
        public static void InsertNewCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "Bruce",
                LastName = "Wayne",
                Country = "Sweden",
                PostalCode = "35248",
                Phone = "0769342312",
                Email = "Bruce@gmail.com"
            };

            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("hej, it worked");
            }
            else
            {
                Console.WriteLine("it did not work");
            }

        }

        /// <summary>
        /// Updates a specific customer with the specific values.
        /// </summary>
        /// <param name="repository">Interface Object</param>
        public static void UpdateCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                CustomerId = 47,
                FirstName = "Rijad",
                LastName = "Hamula",
                Country = "USA",
                PostalCode = "35248",
                Phone = "1343546",
                Email = "Rijad@gmail.com"
            };

            if (repository.UpdateCustomer(test))
            {
                Console.WriteLine("hej, it worked");
            }
            else
            {
                Console.WriteLine("it did not work");
            }

        }
      
        /// <summary>
        /// Selects the customers between two id-values, and get their data.
        /// </summary>
        /// <param name="repository">Interface Object</param>
        public static void SelectCustomersBetweenValues(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetCustomersBetween(32, 59));
        }

        /// <summary>
        /// Selects a specific customer by their id, and get their data.
        /// </summary>
        /// <param name="repository">Interface object</param>
        public static void SelectCustomerById(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerById(3));
        }

        /// <summary>
        /// Selects a specific customer by their first name, and get their data.
        /// </summary>
        /// <param name="repository">Interface object</param>
        public static void SelectByName(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetCustomerByName("Emma"));
        }

        /// <summary>
        /// Prints the customers.
        /// </summary>
        /// <param name="customers">collection of all customers</param>
        public static void PrintCustomers(ICollection customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        /// <summary>
        /// Prints the customer.
        /// </summary>
        /// <param name="customer">Customer object</param>
        public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"---{customer.CustomerId} {customer.FirstName}{customer.LastName} {customer.Country} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}");
        }
    }
}

