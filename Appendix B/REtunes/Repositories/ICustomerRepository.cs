﻿using REtunes.Models;
using System.Collections.Generic;

namespace REtunes.Repositories
{
    public  interface ICustomerRepository
   {
       public Customer GetCustomerById(int id);
       public List<Customer> GetCustomerByName(string name);
       public List<Customer> GetAllCustomers();
       public bool AddNewCustomer(Customer customer);
       public bool UpdateCustomer(Customer customer);
       public List<Customer> GetCustomersBetween(int id1,int id2);
       public List<CustomerCountry> GetCustomersOrderedByCountry();
       public List<CustomerSpender> GetCustomersInvoiceOrderByTotal();
       public List<CustomerGenre> GetCustomersFavoriteGenre(int id);
   }
}
