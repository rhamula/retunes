﻿using REtunes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace REtunes.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Opens a connection to the database, and gets all customers from the database with an SQL query.
        /// </summary>
        /// <returns>A list of all customer objects</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();

            //Selects the customers id, firstname, lastname, country, postal code, phone number and email, from the customer table
            string sql = "SELECT CustomerId, FirstName,LastName, Country,PostalCode,Phone,Email FROM Customer";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return custList;
        }

        /// <summary>
        /// Opens a connection to the database, and gets the customer with a specific id from the database with an SQL query.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>a customer object</returns>
        public Customer GetCustomerById(int id)
        {
            Customer temp = new Customer();
            //Selects the specific customers id, firstname, lastname, country, postal code, phone number and email, from the customer table
            string sql = $"SELECT CustomerId, FirstName,LastName, Country,PostalCode,Phone,Email FROM Customer WHERE CustomerId = {id}";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {

                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return temp;
        }

        /// <summary>
        /// Inserts a customer to the database with the specific values.
        /// </summary>
        /// <param name="customer">customer object</param>
        /// <returns>boolean if the insert worked or not.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            //A query that inserts a customer with the specif values.
            string sql = "INSERT INTO Customer(FirstName,LastName,Country,PostalCode,Phone,Email) " + "VALUES(@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return success;
        }

        /// <summary>
        /// Update a customer to the database with the specific values.
        /// </summary>
        /// <param name="customer">customer object</param>
        /// <returns>boolean if the insert worked or not.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            // Query that updates a specific customer with the specific values.
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode,Phone = @Phone,Email = @Email WHERE CustomerId = @CustomerId";
           
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return success;
        }


        /// <summary>
        /// Opens a connection to the database, and gets the customer with a specific name from the database with an SQL query.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        List<Customer> ICustomerRepository.GetCustomerByName(string name)
        {
            List<Customer> custList = new List<Customer>();
            //Selects the specific customers id, firstname, lastname, country, postal code, phone number and email, from the customer table
            string sql = $"SELECT CustomerId, FirstName,LastName, Country,PostalCode,Phone,Email FROM Customer WHERE FirstName LIKE '{name}'";

            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);

            }
            return custList;
        }

        /// <summary>
        /// Gets the customers between two specific id-values.
        /// </summary>
        /// <param name="id1">first id</param>
        /// <param name="id2">second id</param>
        /// <returns>a list of all customers between the id-values</returns>
        public List<Customer> GetCustomersBetween(int id1, int id2)
        {
            List<Customer> custtList = new List<Customer>();
            //Selects the customers between the two id values.
            string sql = $"SELECT CustomerId, FirstName,LastName, Country,PostalCode,Phone,Email FROM Customer WHERE CustomerId BETWEEN {id1} AND {id2}";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custtList.Add(temp);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return custtList;
        }

        /// <summary>
        /// Gets the number of customers that lives in each country, and sort them by highest to lowest.
        /// </summary>
        /// <returns>a list of all customer objects</returns>
        public List<CustomerCountry> GetCustomersOrderedByCountry()
        {
            //List<int> custtList = new List<int>();
            List<CustomerCountry> custList = new List<CustomerCountry>();
            //Query that selects the number of customers that lives in each country, and sort them by highest to lowest.
            string sql = "SELECT COUNT(Country), Country FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                CustomerCountry customerCountryObj = new CustomerCountry();
                                customerCountryObj.NumberOfCustomers = reader.GetInt32(0);
                                customerCountryObj.CountryName = reader.GetString(1);
                                custList.Add(customerCountryObj);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return custList;
        }

        /// <summary>
        /// Gets all customers and how much the have spent.
        /// </summary>
        /// <returns>list of customer objects</returns>
        public List<CustomerSpender> GetCustomersInvoiceOrderByTotal()
        {
            //List<int> custtList = new List<int>();
            List<CustomerSpender> custList = new List<CustomerSpender>();
            string sql = "SELECT SUM(Invoice.Total), Customer.FirstName,Customer.LastName FROM Invoice INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId GROUP BY Customer.FirstName, Customer.LastName ORDER BY SUM(Invoice.Total) DESC";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {

                                CustomerSpender customerSpenderObj = new CustomerSpender();
                                customerSpenderObj.Total = (double)reader.GetDecimal(0);
                                customerSpenderObj.FullName = $"{reader.GetString(1)} {reader.GetString(2)}";
                                custList.Add(customerSpenderObj);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return custList;
        }

        /// <summary>
        /// Gets a specific customers favorite genre.
        /// </summary>
        /// <param name="id">the id of the specific customer</param>
        /// <returns>a list of the customers favorite genres</returns>
        public List<CustomerGenre> GetCustomersFavoriteGenre(int id)
        {
            List<CustomerGenre> custList = new List<CustomerGenre>();

            //Selects the customers listened genres, by joining 5 tables.
            string sql = "SELECT g.Name, COUNT(g.GenreId) AS GenreCount " +
            "FROM Genre as g " +
            "INNER JOIN Track as t ON g.GenreId = t.GenreId " +
            "INNER JOIN InvoiceLine as il ON t.TrackId = il.TrackId " +
            "INNER JOIN Invoice as i ON i.InvoiceId = il.InvoiceId " +
            "INNER JOIN Customer as c ON i.CustomerId = c.CustomerId " +
             $"WHERE c.CustomerId = {id} " +
            "GROUP BY g.Name " +
                "ORDER BY GenreCount DESC";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                CustomerGenre customerGenreObj = new CustomerGenre();

                                customerGenreObj.GenreCount = reader.GetInt32(1);
                                customerGenreObj.Genre = $"{reader.GetString(0)}";
                                custList.Add(customerGenreObj);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return custList;
        }
    }
}
