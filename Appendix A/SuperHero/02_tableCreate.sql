USE SuperHeroDb

CREATE TABLE SuperHero (
	superhero_id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	superhero_name nvarchar(50) NULL,
	superhero_alias nvarchar(50) NULL,
	superhero_origin nvarchar(50) NULL
);

CREATE TABLE Assistant (
	assistant_id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	assistant_name nvarchar(50) NULL,
);

CREATE TABLE Power (
	power_id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	power_name nvarchar(50) NULL,
	power_description nvarchar(100) NULL
);