USE SuperHeroDb

INSERT INTO SuperHero (superhero_name, superhero_alias, superhero_origin) VALUES ('Bruce Wayne', 'Batman', 'Human');

INSERT INTO SuperHero (superhero_name, superhero_alias, superhero_origin) VALUES ('Peter Parker', 'Spiderman', 'Human');

INSERT INTO SuperHero (superhero_name, superhero_alias, superhero_origin) VALUES ('Bruce Banner', 'The Hulk', 'Half Human');
