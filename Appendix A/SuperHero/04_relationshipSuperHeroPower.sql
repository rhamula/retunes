USE SuperHeroDb

CREATE TABLE SuperHeroPower (
	superhero_id int FOREIGN KEY REFERENCES SuperHero(superhero_id),
	power_id int FOREIGN KEY REFERENCES Power(power_id)
)