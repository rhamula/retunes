USE SuperHeroDb

ALTER TABLE dbo.Assistant ADD superhero_id INT NOT NULL;

ALTER TABLE Assistant ADD FOREIGN KEY (superhero_id) REFERENCES SuperHero(superhero_id);